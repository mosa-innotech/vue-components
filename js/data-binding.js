var titleUpdate = new Vue({
  el: '#title-update',
  data: {
    theTitle: 'Data Binding'

  }
})

var myJournal = new Vue({
  el: '#my-journal',
  data: {
    journalTitle: 'Input journal entry:',
    myJournalEntry: ''
  }
})

new Vue({
  el: '#introduction',
  data: {
    introMessage: 'This is an introduction'
  }
})

console.log(myJournal);
