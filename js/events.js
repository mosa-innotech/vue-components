var d = new Date();
var currentYear = d.getFullYear();
var myName = "Mosa";

new Vue({
  el: '#title',
  data: {
    title: 'Welcome to Vue.js - Events'
  }
})

new Vue({
  el: '#footer',
  data: {
    text: '\u00A9 ' + currentYear + ' - Welcome to Vue.js '
  }
})


new Vue({
  el: '#button-1-div'
})

new Vue({
  el: '#button-2-div'
})

new Vue({
  el: '#para-div'
})

new Vue({
  el: '#button-3-div',
  methods: {
      sayByeBye(){
          alert('Bye ' + name);
      }
  }
})
