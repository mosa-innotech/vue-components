var d = new Date();
var currentYear = d.getFullYear();

new Vue({
  el: '#title',
  data: {
    title: 'Vue Components & Data Binding'
  }
})

new Vue({
  el: '#footer',
  data: {
    text: '\u00A9 ' + currentYear + ' - Welcome to Vue.js '
  }
})
