new Vue({
    el: '#profile',
    data: {
        profile: {
            name: 'Leeroy Jenkins',
            avatar: 'https://www.opencollege.info/wp-content/uploads/2016/02/relaxation-skills.jpg',
            bio: 'Leeeeeeeeerrrrrrrooooyyyyyy Jenkins'
        }
    },
    methods: {
        changePic(){
            this.profile.avatar = 'https://helpx.adobe.com/ca/stock/how-to/visual-reverse-image-search/_jcr_content/main-pars/image.img.jpg/visual-reverse-image-search-v2_1000x560.jpg'
        }
    }
});
