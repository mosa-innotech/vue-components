var d = new Date();
var currentYear = d.getFullYear();

new Vue({
  el: '#title',
  data: {
    title: 'Welcome to Vue.js - V-for'
  }
})

new Vue({
  el: '#footer',
  data: {
    text: '\u00A9 ' + currentYear + ' - Welcome to Vue.js '
  }
})

var taskList = new Vue({
  el: '#tasks-div',
  data: {
    newTask: '',
    tasks: [
        'eat',
        'sleep',
        'repeat'
    ]
},
  methods: {
      addTask(){
         this.tasks.push(this.newTask);
     },
      removeTask(){
         this.tasks.pop();
      }
  }
})

console.log(taskList);
