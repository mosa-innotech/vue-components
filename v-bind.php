<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Vue Components</title>
        <link rel="stylesheet" href="css/vue.css">
    </head>
    <body>
        <h1 id="title">{{ title }}</h1>
        <br />
        <ul>
            <li>
                <a href="v-bind.php">V-Binding</a>
            </li>
            <li>
                <a href="">Class Binding</a>
            </li>
            <li>
                <a href="">Components</a>
            </li>
            <li>
                <a href="">Nested Components</a>
            </li>
        </ul>

        <br /><br /><br /><br />

        <div id="profile" style="border: 1px solid #fff; padding: 30px; border-radius: 10px; width: 300px;">
            <img v-bind:src="profile.avatar" width="300" />
            <h3>{{ profile.name }}</h3>
            <p>{{ profile.bio }}</p>
            <br />
            <br />
            <br />
            <br />
            <button class="btn btn-primary" @click='changePic'>Change Pic</button>
        </div>

        <br /><br />
        <?php include ("partials/footer.php");?>

        <script type="text/javascript" src="js/vue.min.js"></script>
        <script type="text/javascript" src="js/index.js"></script>
        <script type="text/javascript" src="js/v-bind.js"></script>
    </body>
</html>
